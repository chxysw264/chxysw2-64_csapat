﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transport.ViewModels;

namespace Transport.JsonModel
{
    public class ServerJSON
    {

        public class requestJSON
        {
            public GeoCoordinate worldcoordinate { get; set; }
            public string from_station { get; set; }
            public string to_station { get; set; }
        }

        public class responseJSON
        {

            public double walking_distance { get; set; }
            public int walking_duration { get; set; }

            public List<BusSchedule> line { get; set; }
            public BusSchedule closest_station { get; set; }
        }

    }
}
