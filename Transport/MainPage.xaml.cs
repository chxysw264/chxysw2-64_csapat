﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.Devices.Geolocation;
using System.Device.Location;
using System.Windows.Shapes;
using System.Windows.Media;
using Microsoft.Phone.Maps.Controls;
using Microsoft.Phone.Maps.Services;
using System.Text;
using System.IO;
using System.IO.IsolatedStorage;
using System.Threading.Tasks;
using Windows.Storage;
using Transport.ServiceManager;
using System.Net.Sockets;
using Microsoft.Phone.Net.NetworkInformation;
using Transport.JsonModel;
using Newtonsoft.Json;
using Transport.ViewModels;

namespace Transport
{
    public partial class MainPage : PhoneApplicationPage
    {


        #region Attributes

        GeoCoordinate myGeoCoordinate;
        Geolocator myGeolocator;
        Geoposition myGeoposition;
        Geocoordinate myGeocoordinate;
        RouteQuery routeQuery;
        ServerJSON.responseJSON serverresponse;
        Panorama pan;
        MapLayer myLocationLayer;
        MapRoute calculatedMapRoute;
        MapLayer busStopsLayer;

        #endregion




        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
            pan = (Panorama)this.FindName("panorama");
            busStopsLayer = new MapLayer();
        }

        // Load data for the ViewModel Items
        List<string> states = new List<string>();

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }

            //add álloméánevek
            
            string path = "Assets\\buszmegallokvp.csv";
            var reader = new StreamReader(File.OpenRead(path));
            
            
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                var values = line.Split('"');

                states.Add(values[1]);
                
            }

            


            start.ItemsSource = states;
            stop.ItemsSource = states;

        }


        private void LongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ViewModels.ItemViewModel item = (ViewModels.ItemViewModel)buslist.SelectedItem;
            MessageBox.Show(item.LineTwo);

        }

        private int startSzamlalo = 0;
        private int stopSzamlalo = 0;

        private void start_GotFocus(object sender, RoutedEventArgs e)
        {
            if (startSzamlalo < 1)
            {
                start.Text = "";
                startSzamlalo += 1;
            }

        }
        private void stop_GotFocus(object sender, RoutedEventArgs e)
        {
            if (stopSzamlalo < 1)
            {
                stop.Text = "";
                stopSzamlalo += 1;
            }

        }



        void query_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
            
            foreach (var item in e.Result)
            {
            start.Text =
                item.Information.Address.City + " " +
                item.Information.Address.Street + " " +
                item.Information.Address.HouseNumber
                ;
            }
        }


        private void getLocationButton_Click(object sender, RoutedEventArgs e)
        {
            getMyGeoCoordinate();
            if(myGeoCoordinate != null){
                ReverseGeocodeQuery query = new ReverseGeocodeQuery()
                {
                    GeoCoordinate = myGeoCoordinate
                };
                query.QueryCompleted += query_QueryCompleted;
                query.QueryAsync();
            }
            else
            {
                MessageBox.Show("Még nincs pozíció... Próbáld meg újra!");
            }


        }

        private async void getMyGeoCoordinate()
        {

            myGeolocator = new Geolocator();
            if (myGeolocator.LocationStatus == PositionStatus.Disabled)
            {
                MessageBox.Show("Lokalizáció ki van kapcsolva!");
            }
            else
            {
                MessageBox.Show("Koordináták keresése folyamatban!");
                myGeoposition = await myGeolocator.GetGeopositionAsync();
                myGeocoordinate = myGeoposition.Coordinate;
                myGeoCoordinate =
                Transport.LocationControl.ShowMyLocationOnMap.CoordinateConverter.ConvertGeocoordinate(myGeocoordinate);
            }

        }

        private void ShowBusStops(List<GeoCoordinate> geoPoints)
        {
            if (busStopsLayer != null)
            {
                busStopsLayer.Clear();
            }

            foreach (var item in geoPoints)
            {
                Ellipse circle = new Ellipse();
                circle.Fill = new SolidColorBrush(Colors.Yellow);
                circle.Height = 10;
                circle.Width = 10;
                circle.Opacity = 50;

                MapOverlay busStopOverlay = new MapOverlay();
                busStopOverlay.Content = circle;
                busStopOverlay.PositionOrigin = new Point(0.5, 0.5);
                busStopOverlay.GeoCoordinate = item;

                busStopsLayer.Add(busStopOverlay);
            }
            mapWithRouteAndDirection.Layers.Add(busStopsLayer);
        }

        private void ShowMyLocationOnTheMap()
        {
            if(myLocationLayer != null)
            {
                myLocationLayer.Clear();
            }
            
            getMyGeoCoordinate();
            
            // Get my current location.
            if (myGeoCoordinate != null)
            {

                this.mapWithRouteAndDirection.Center = myGeoCoordinate;
                this.mapWithRouteAndDirection.ZoomLevel = 17;

                // Create a small circle to mark the current location.
                Ellipse myCircle = new Ellipse();
                myCircle.Fill = new SolidColorBrush(Colors.Red);
                myCircle.Height = 20;
                myCircle.Width = 20;
                myCircle.Opacity = 50;

                // Create a MapOverlay to contain the circle.
                MapOverlay myLocationOverlay = new MapOverlay();
                myLocationOverlay.Content = myCircle;
                myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
                myLocationOverlay.GeoCoordinate = myGeoCoordinate;

                // Create a MapLayer to contain the MapOverlay.
                myLocationLayer = new MapLayer();
                myLocationLayer.Add(myLocationOverlay);

                // Add the MapLayer to the Map.
                mapWithRouteAndDirection.Layers.Add(myLocationLayer);
                MessageBox.Show("A pirossal jelzett helyen állsz!");
            }
            else
            {
                MessageBox.Show("Még nincs pozíció. Próbáld meg újra!");
            }
        }


        private void Map_Loaded(object sender, RoutedEventArgs e)
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "b39db769-4ab7-41fe-939d-ad4e1d269f1d";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "dq1iMGtnLrewZpfQMSzHWw";
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            Boolean StartOK = false;
            Boolean StopOK = false;
            SocketManager server = new SocketManager();
            server.Connect("172.21.1.65", 6414);

            foreach (var item in states)
            {
                if(item.Equals(stop.Text) )
                {
                    StopOK = true;
                }
                else if((item.Equals(start.Text) || myGeoCoordinate != null))
                {
                    StartOK = true;
                }
            }

            
            if(StartOK && StopOK)
            {
                ServerJSON.requestJSON request = new ServerJSON.requestJSON();
                request.worldcoordinate = myGeoCoordinate;
                request.from_station = start.Text;
                request.to_station = stop.Text;
                server.Send(JsonConvert.SerializeObject(request) + "#");
                string serverResponseString = server.Receive();
                try
                {
                    serverresponse = JsonConvert.DeserializeObject<ServerJSON.responseJSON>(serverResponseString);

                    displayRoute();
                    pan.DefaultItem = pan.Items[2];
                    if (serverresponse.closest_station != null)
                    {
                        infoTblock.Text = "Busz szám: " + serverresponse.line[0].Lvonal.ToString() +
                                        "\nLegkölelebbi állomás: " + serverresponse.closest_station.Lnev +
                                        "\nTávolság: " + serverresponse.walking_distance.ToString() +
                                        "\nÁllomások:\n"
                        ;
                        foreach (var item in serverresponse.line)
                        {
                            infoTblock.Text += "\t" + item.Lnev + "\n";
                        }

                    }
                    else
                    {
                        infoTblock.Text = "Busz szám: " + serverresponse.line[0].Lvonal.ToString() +
                                        "\nLegkölelebbi állomás: " + start.Text +
                                        "\nTávolság: " + serverresponse.walking_distance.ToString() +
                                        "\nÁllomások:\n"
                        ;
                        foreach (var item in serverresponse.line)
                        {
                            infoTblock.Text += "\t" + item.Lnev + "\n";
                        }
                    }


                }
                catch (JsonReaderException)
                {
                    MessageBox.Show("Server ERROR");
                }
            }
            else
            {
                MessageBox.Show("Rossz megállót adtál meg!");
            }
            

            
        }

        
        
        void routeQuery_QueryCompleted(object sender, QueryCompletedEventArgs<Route> e)
        {
            bool deleted = false;
            if(e.Error == null)
            {
                Route theRoute = e.Result;
                if(calculatedMapRoute != null)
                {
                    deleted = mapWithRouteAndDirection.RemoveRoute(calculatedMapRoute);
                }
                if(deleted || calculatedMapRoute == null)
                {
                    calculatedMapRoute = new MapRoute(theRoute);
                    mapWithRouteAndDirection.AddRoute(calculatedMapRoute);
                }

            }

        }


        private void displayRoute()
        {
            List<GeoCoordinate> busStops = new List<GeoCoordinate>();
            Boolean ok = false;
            routeQuery = new RouteQuery();
            routeQuery.QueryCompleted += routeQuery_QueryCompleted;
            int startindex = -1, stopindex = -1, i = 0;

            if (!routeQuery.IsBusy)
            {
                foreach (var item in serverresponse.line)
                {
                    if(item.Lnev.Equals(start.Text))
                    {
                        startindex = i;
                    }
                    else if (item.Lnev.Equals(stop.Text))
                    {
                        stopindex = i;
                    }
                    i++;
                }

                
                List<GeoCoordinate> routeCoordinates = new List<GeoCoordinate>();

                if (startindex < stopindex && startindex!=-1 && stopindex!=-1)
                {
                    
                    foreach (var item in serverresponse.line)
                    {
                        if (item.Lnev.Equals(start.Text))
                        {
                            ok = true;
                        }
                        else if (item.Lnev.Equals(stop.Text))
                        {
                            ok = false;
                            routeCoordinates.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                            busStops.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                        }
                        if (ok == true)
                        {
                            routeCoordinates.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                            busStops.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                        }

                    }
                }
                else if (startindex > stopindex && startindex != -1 && stopindex != -1)
                {
                    
                    serverresponse.line.Reverse();
                    foreach (var item in serverresponse.line)
                    {
                        if (item.Lnev.Equals(start.Text))
                        {
                            ok = true;
                        }
                        else if (item.Lnev.Equals(stop.Text))
                        {
                            ok = false;
                            routeCoordinates.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                            busStops.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                        }
                        if (ok == true)
                        {
                            routeCoordinates.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                            busStops.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                        }
                    }
                }
                else if(startindex == -1)
                {
                    serverresponse.line.Reverse();
                    foreach (var item in serverresponse.line)
                    {
                        if (item.Lnev.Equals(serverresponse.closest_station.Lnev))
                        {
                            ok = true;
                        }
                        else if (item.Lnev.Equals(stop.Text))
                        {
                            ok = false;
                            routeCoordinates.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                            busStops.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                        }
                        if (ok == true)
                        {
                            routeCoordinates.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                            busStops.Add(new GeoCoordinate(item.Lgpsy, item.Lgpsx));
                        }
                    }
                }
                try
                {
                    ShowBusStops(busStops);
                }
                catch (Exception)
                {
                }
                routeQuery.Waypoints = routeCoordinates;
                routeQuery.QueryAsync();

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ShowMyLocationOnTheMap();
        }


    }
}