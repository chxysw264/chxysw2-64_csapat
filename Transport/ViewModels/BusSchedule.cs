﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport.ViewModels
{
    public class BusSchedule
    {

        #region Attributes
        



        #endregion

        #region Properties

        public int Lvonal
        {
            get;
            set;
        }
        public int Lsorsz
        {
            get;
            set;
        }
        public int Liora
        {
            get;
            set;
        }
        public int Liperc
        {
            get;
            set;
        }
        public int Leora
        {
            get;
            set;
        }
        public int Leperc
        {
            get;
            set;
        }
        public double Lgpsx
        {
            get;
            set;
        }
        public double Lgpsy
        {
            get;
            set;
        }
        public string Lnev
        {
            get;
            set;
        }
        public int Lkod
        {
            get;
            set;
        }
        public string Lodavi
        {
            get;
            set;
        }
        public int Ljarat
        {
            get;
            set;
        }
        public string Lkkorl
        {
            get;
            set;
        }
        public int Lhtml
        {
            get;
            set;
        }
        public int Lpdf
        {
            get;
            set;
        }
        public string Lkorlne
        {
            get;
            set;
        }
        public string Lkorlna
        {
            get;
            set;
        }
        public int Ljp
        {
            get;
            set;
        }
        public int Lsm
        {
            get;
            set;
        }

        public int Lid
        {
            get;
            set;
        }

        public int LnextId
        {
            get;
            set;
        }

        public int LprevId
        {
            get;
            set;
        }

        public int Lsuly
        {
            get;
            set;
        }

        #endregion

        #region Private and Protected Methods



        #endregion

        #region Public Functions and Events



        #endregion
        
    }
}
