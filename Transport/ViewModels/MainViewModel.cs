﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Transport.Resources;

namespace Transport.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            this.Items = new ObservableCollection<ItemViewModel>();
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<ItemViewModel> Items { get; private set; }

        private string _sampleProperty = "Sample Runtime Property Value";
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding
        /// </summary>
        /// <returns></returns>
        public string SampleProperty
        {
            get
            {
                return _sampleProperty;
            }
            set
            {
                if (value != _sampleProperty)
                {
                    _sampleProperty = value;
                    NotifyPropertyChanged("SampleProperty");
                }
            }
        }

        /// <summary>
        /// Sample property that returns a localized string
        /// </summary>
        public string LocalizedSampleProperty
        {
            get
            {
                return AppResources.SampleProperty;
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            // Sample data; replace with real data
            this.Items.Add(new ItemViewModel() { LineOne = "1-es busz", LineTwo = "Autóbusz-állomás – Laktanya – Vasútállomás – Jutaspuszta – Dózsa Gy. tér – Pápai u. ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "1-es busz", LineTwo = "Pápai u. ford – Dózsa Gy. tér – Jutaspuszta – Vasútállomás – Laktanya – Autóbusz-állomás"});
            this.Items.Add(new ItemViewModel() { LineOne = "2-es busz", LineTwo = "Vasútállomás – Autóbusz-állomás – Színház – Endrődi S. ltp. – Hotel - Vasútállomás"});
            this.Items.Add(new ItemViewModel() { LineOne = "3-as busz", LineTwo = "Csererdő – Dózsa Gy. tér – Hotel – Autóbusz-állomás – Petőfi S. u. – Haszkovó ford."});
            this.Items.Add(new ItemViewModel() { LineOne = "3-as busz", LineTwo = "Haszkovó ford. – Petőfi S. u. – Autóbusz-állomás – Hotel – Dózsa Gy. tér – Csererdő"});
            this.Items.Add(new ItemViewModel() { LineOne = "4-es busz", LineTwo = "Vasútállomás - Munkácsy M. u. – Autóbusz-állomás – Hotel – Vámosi u. ford."});
            this.Items.Add(new ItemViewModel() { LineOne = "4-es busz", LineTwo = "Vámosi u. ford. – Hotel – Autóbusz-állomás – Munkácsy M. u. – Vasútállomás" });
            this.Items.Add(new ItemViewModel() { LineOne = "5-ös busz", LineTwo = "Kádártai u. ford. – Cholnoky ford. – Hotel – Dózsa Gy. tér – Tüzér u. – Tüzér u. ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "5-ös busz", LineTwo = "Tüzér u. ford. – Tüzér u. – Dózsa Gy. tér – Hotel – Cholnoky ford. – Kádártai u. ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "6-os busz", LineTwo = "Vámosi u. ford. – Egry J. u. – Hotel – Kádártai u. – Haszkovó ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "6-os busz", LineTwo = "Haszkovó ford. – Kádártai u. – Hotel – Egry J. u. – Vámosi u. ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "7-es busz", LineTwo = "Haszkovó ford. – Autóbusz-állomás – Cholnoky ltp. – Radnóti M. tér – Haszkovó ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "8-as busz", LineTwo = "Haszkovó ford. – Kádártai u. – Cholnoky ltp. – Egyetemváros – Pápai u. ford. – (Csatárhegy út)" });
            this.Items.Add(new ItemViewModel() { LineOne = "8-as busz", LineTwo = "Pápai u. ford. – (Csatárhegy út) – Egyetemváros - Cholnoky ltp. – Kádártai u. – Haszkovó ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "10-es busz", LineTwo = "Pápai u. ford. – Hotel – Vilonyai u. – Cholnoky ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "10-es busz", LineTwo = "Cholnoky ford. – Vilonyai u. – Hotel – Pápai u. ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "11-es busz", LineTwo = "Vasútállomás – Haszkovó u. – Cholnoky ltp. – Egry J. u. – Vámosi u. ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "11-es busz", LineTwo = "Vasútállomás – Haszkovó u. – Cholnoky ltp. – Egry J. u. – Vámosi u. ford. valamiben más :D" });
            this.Items.Add(new ItemViewModel() { LineOne = "12-es busz", LineTwo = "Autóbusz-állomás – Pajta u. – Dózsa Gy. tér – Jókai M. u. - Autóbusz-állomás " });
            this.Items.Add(new ItemViewModel() { LineOne = "13-as busz", LineTwo = "Kádártai úti ford. – Autóbusz-állomás – Dózsa Gy. tér – Kádártai úti ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "18-as busz", LineTwo = "Haszkovó ford. – Munkácsy M. u. – Jutasi úti ltp. – Kádártai út – Papvásár u." });
            this.Items.Add(new ItemViewModel() { LineOne = "19-es busz", LineTwo = "Színház – Cholnoky ltp. – Jutasi úti ltp. – Hotel – Dózsa Gy. tér – Pápai u. ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "22-es busz", LineTwo = "Autóbusz-állomás – Déli Intézményközpont – Magtár – Laci-major" });
            this.Items.Add(new ItemViewModel() { LineOne = "22-es busz", LineTwo = "Laci-major – Magtár – Déli Intézményközpont – Autóbusz-állomás" });
            this.Items.Add(new ItemViewModel() { LineOne = "23-as busz", LineTwo = "Autóbusz-állomás – Kádárta v. mh. – Gyulafirátót, ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "23-as busz", LineTwo = "Gyulafirátót, ford. – Kádárta v. mh. – Autóbusz-állomás" });
            this.Items.Add(new ItemViewModel() { LineOne = "24-es busz", LineTwo = "Autóbusz-állomás – Kádárta, v. mh." });
            this.Items.Add(new ItemViewModel() { LineOne = "24-es busz", LineTwo = "Kádárta, v. mh. – Autóbusz-állomás" });
            this.Items.Add(new ItemViewModel() { LineOne = "25-ös busz", LineTwo = "Gyulafirátót, ford. – Kádárta – Hotel – Dózsa Gy. tér – Papvásár u. " });
            this.Items.Add(new ItemViewModel() { LineOne = "26-os busz", LineTwo = "Kádárta, v. mh. – Megyeház tér" });
            this.Items.Add(new ItemViewModel() { LineOne = "32-es busz", LineTwo = "Cholnoky ford. – Fecske u. – Haszkovó u. – Csererdő" });
            this.Items.Add(new ItemViewModel() { LineOne = "32-es busz", LineTwo = "Csererdő – Haszkovó u. – Fecske u. – Cholnoky ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "34-es busz", LineTwo = "Vámosi u. ford. – Egry J. u. – Volán telep – Csererdő" });
            this.Items.Add(new ItemViewModel() { LineOne = "34-es busz", LineTwo = "Csererdő – Volán telep – Egry J. u. – Vámosi u. ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "35-ös busz", LineTwo = "Autóbusz-állomás – Haszkovó u. – Csererdő " });
            this.Items.Add(new ItemViewModel() { LineOne = "35-ös busz", LineTwo = "Csererdő – Haszkovó u. – Autóbusz-állomás " });
            this.Items.Add(new ItemViewModel() { LineOne = "50-es busz", LineTwo = "Autóbusz-állomás – Színház – Dózsa Gy. tér – Tüzér u. – Tüzér u. ford." });
            this.Items.Add(new ItemViewModel() { LineOne = "50-es busz", LineTwo = "Tüzér u. ford. – Tüzér u. – Dózsa Gy. tér – Színház – Autóbusz-állomás" });

            this.IsDataLoaded = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}